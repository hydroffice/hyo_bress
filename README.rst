HydrOffice BRESS
================

.. image:: https://github.com/hydroffice/hyo_bress/raw/master/py/media/favicon.png
    :alt: logo

* Code: `GitHub repo <https://github.com/hydroffice/hyo_bress>`_
* Project page: `url <http://www.hydroffice.org/oceano/>`_
* Download page: `url <https://bitbucket.org/hydroffice/hyo_bress/downloads/>`_
* License: LGPLv3 license (See `LICENSE <https://bitbucket.org/ccomjhc/hyo_bress/raw/master/py/LICENSE>`_)

|

General Info
------------

.. image:: https://api.codacy.com/project/badge/Grade/dc7bcf895e5d40a0b03c7bf55e21626d
    :target: https://www.codacy.com/app/hydroffice/hyo_bress
    :alt: Codacy

HydrOffice is a research development environment for ocean mapping. It provides a collection of hydro-packages, each of them dealing with a specific issue of the field.
The main goal is to speed up both algorithms testing and research-2-operation (R2O).

The Bathy- and Reflectivity-based Estimator for Seafloor Segments (BRESS) hydro-package provides means to segment a seafloor areas.


Dependencies
------------

For the library, you will need:

* `Eigen <http://eigen.tuxfamily.org/index.php?title=Main_Page>`_
* `OpenCV <http://opencv.org/>`_

For the Python binding, you will need:

* `Python <https://www.python.org/>`_ *[>=3.5]*
* `NumPy <http://www.numpy.org/>`_

For running some of the example scripts, you might also need:

* `Matplotlib <http://matplotlib.org/>`_

For running the tests, install:

* `Google Test Adapter <https://marketplace.visualstudio.com/items?itemName=ChristianSoltenborn.GoogleTestAdapter>`_

For debugging, you may want to install:

* `Visual Leak Detector <https://vld.codeplex.com/>`_
